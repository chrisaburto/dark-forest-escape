﻿using UnityEngine;
using System.Collections;

public class DestroyScript : MonoBehaviour
{
    public float timeBeforeDestroy = 8.0f;

    void OnEnable()
    {
        Invoke("Destroy", timeBeforeDestroy);
    }

	void Destroy ()
	{
		gameObject.SetActive (false);
	}

	void OnDisable ()
	{
		CancelInvoke ();
	}
}
