﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewPoolerScript : MonoBehaviour {
    public static NewPoolerScript current;
    public GameObject[] pooledObj;
    public int pooledAmount = 10;
    public bool grow = true;

    List<GameObject> pooledObjs;

    void Awake ()
    {
        current = this;
    }

	// Use this for initialization
	void Start () {
	    pooledObjs = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(pooledObj[Random.Range(0, pooledObj.Length)], transform.position, transform.rotation) as GameObject;
            obj.SetActive(false);
            pooledObjs.Add(obj);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjs.Count; i++)
        {
            if (!pooledObjs[i].activeInHierarchy)
            {
                return pooledObjs[i];
            }
        }

        if (grow)
        {
            GameObject obj = (GameObject)Instantiate(pooledObj[Random.Range(0, pooledObj.Length)], transform.position, transform.rotation);
            pooledObjs.Add(obj);

            return obj;
        }

        return null;
    }
}
