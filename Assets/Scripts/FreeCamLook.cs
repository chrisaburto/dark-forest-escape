﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class FreeCamLook : Pivot 
{
	/*varies speeds and max movements for camera*/
	[SerializeField] private float moveSpeed = 5.0f;
	[SerializeField] private float turnSpeed = 1.5f;
	[SerializeField] private float turnSmoothing = 0.1f;
	[SerializeField] private float tiltMax = 75.0f;
	[SerializeField] private float tiltMin = 45.0f;
	[SerializeField] private bool lockCursor = false;

	/*angles for looking ang tilting through free cam*/
	private float lookAngle;
	private float tiltAngle;

	/*look distance of cam*/
	private const float LookDistance = 100.0f;

	/**/
	private float smoothX = 0.0f;
	private float smoothY = 0.0f;

	/*how smooth cam will turn*/
	private float smoothXvelocity = 0.0f;
	private float smoothYvelocity = 0.0f;

	void Start()
	{
		/*hides cursor druring run time*/
		Cursor.visible = false;
	}
	protected override void Awake()
	{
		/*calling awake from super class Pivot*/
		base.Awake();

		/*option to lock cursor during run time*/
		Screen.lockCursor = lockCursor;

		/*main camera is child component of pivot, and parent 
		 of pivot is Camera*/
		cam = GetComponentInChildren<Camera>().transform;

		/*setting pivot to cam parent*/
		pivot = cam.parent;
	}

	protected override void Update()
	{
		/*calling Update from super class Pivot*/
		base.Update ();

		/*run this function*/
		HandleRotationMovement();

		/*if we press left mouse button, lock cursor*/
		if(lockCursor && Input.GetMouseButtonUp(0))
		{
			Screen.lockCursor = lockCursor;
		}
	}

	void OnDisable()
	{
		Screen.lockCursor = false;
	}

	protected override void Follow(float deltaTime)
	{
		/*changes/moves these vectors*/
		transform.position = Vector3.Lerp(transform.position, target.position, deltaTime * moveSpeed);
	}

	void HandleRotationMovement()
	{
		float x = Input.GetAxis ("MouseX");
		float y = Input.GetAxis("MouseY");

		if(turnSmoothing > 0)
		{
			smoothX = Mathf.SmoothDamp(smoothX,x, ref smoothXvelocity, turnSmoothing);
			smoothY = Mathf.SmoothDamp(smoothY,y, ref smoothYvelocity, turnSmoothing);
		}
		else
		{
			smoothX = x;
			smoothY = y;
		}

		lookAngle += smoothX*turnSpeed;

		transform.rotation = Quaternion.Euler(0.0f, lookAngle, 0.0f);

		tiltAngle -= smoothY * turnSpeed;
		tiltAngle = Mathf.Clamp(tiltAngle, -tiltMin, tiltMax);

		pivot.localRotation = Quaternion.Euler (tiltAngle, 0.0f, 0.0f);
	}
}