﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour
{
    public float speed = 2.0f;

    private float velocity;
    private GameObject player;
    private PickupInstantiation script;
	
	void Start () 
	{
        velocity = speed * Time.deltaTime;
        player = GameObject.FindGameObjectWithTag("Player");
        script = player.GetComponent<PickupInstantiation>();
	}

	void Update () 
	{
        transform.position -= transform.forward * velocity;
		//transform.Rotate(new Vector3(15,30,45) * Time.deltaTime);	
        if (transform.position.z < -15)
        {
            Destroy(this.gameObject);
            script.healthInstantiated = false;
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            script.healthInstantiated = false;
            c.gameObject.GetComponent<playerActions>().health++;
			Debug.Log ("health collected. Health = " +  c.gameObject.GetComponent<playerActions>().health);
            Destroy(this.gameObject);
        }
    }
}