﻿using UnityEngine;
using System.Collections;

public class TerrainDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit (Collider c)
    {
        if (c.gameObject.tag == "Plane")
        {
            Destroy(c.gameObject);
        }
    }
}
