﻿using UnityEngine;
using System.Collections;

public class AIScript : MonoBehaviour {

    public Transform[] wayPath; //array of waypoints
    public float waySpeed = 3.0f;   //speed of patrol
    public bool wayLoop = true; //if waypoints loop
    public float wayLook = 6.0f;    
    public float wayPause = 0.0f;   //pause on each waypoint

    //private GameObject player;
    private float currentTime = 0.0f;
    private int currentPoint = 0;   //current array position
    private CharacterController character;  //AI must have a character controler attached

	// Use this for initialization
	void Start () 
	{
        character = gameObject.AddComponent<CharacterController>();
        //player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {

		if (currentPoint < wayPath.Length)
        {
            Patrol();
        }   //patrols if the current point isnt the last point
        else
        {
            if (wayLoop)
            {
                currentPoint = 0;
            }   //loops back to 0 if wayLoop is true
        }
	}

    void Patrol () 
	{
        Vector3 currentPlace = wayPath[currentPoint].localPosition;
        //Debug.Log(currentPlace);
        currentPlace.y = transform.localPosition.y;
        Vector3 direction = currentPlace - transform.localPosition;
        float step = waySpeed * Time.deltaTime;

        if (direction.magnitude < 0.5)
        {
            if (currentTime == 0)
            {
                currentTime = Time.time;
            }
            if ((Time.time - currentTime) >= wayPause)
            {
                currentPoint++;
                currentTime = 0;
            }
        }
        else
        {
            Quaternion rotation = Quaternion.LookRotation(currentPlace - transform.localPosition);
            transform.rotation = Quaternion.Slerp(transform.localRotation, rotation, Time.deltaTime * wayLook);
            character.Move(direction.normalized * step);
            //Debug.Log("Moving");
        }
    }

    /*void OnTriggerStay(Collider c)
    {
        Vector3 playerPosition = player.transform.position;
        playerPosition.y = transform.localPosition.y;
        Vector3 direction = playerPosition - transform.localPosition;
        float step = 1 * Time.deltaTime;

        if (c.gameObject.tag == "PlayerTrigger")
        {
            Vector3 newDirection = Vector3.RotateTowards(direction, playerPosition, step, 0.0f);
            transform.position = Vector3.MoveTowards(transform.localPosition, playerPosition, step);
            transform.rotation = Quaternion.LookRotation(newDirection);
            character.Move(direction.normalized * step);
        }
    }*/
}
