﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIScript : MonoBehaviour {

    private string score;
    private string time;
    private string health;
    private string ammo;

    public float timer;

    private GameObject player;
    private playerActions script;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        script = player.GetComponent<playerActions>();

        Time.timeScale = 1;
	}

	// Update is called once per frame
	void Update () {
        timer = Time.time;

        score = "Gold: " + script.gold;
        health = "Health: " + script.health;
        ammo = "Ammo: " + script.ammo;
        time = "Time: " + timer.ToString("F") + "s";
	}

    void OnGUI()
    {
        if (script.health > 0)
        {
			GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 100, 20), "+");
            GUI.Label(new Rect(0, 25, 100, 20), score);
            GUI.Label(new Rect(Screen.width - 100, 0, 100, 20), time);
            GUI.Label(new Rect(0, 0, 100, 20), health);
            GUI.Label(new Rect(0, 50, 100, 20), ammo);
        }
        else if (script.health == 0 || !script)
        {
            Time.timeScale = 0;

            GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 200, 20), "Final Time: " + timer.ToString("F") + "s");
            GUI.Label(new Rect(Screen.width / 2, (Screen.height / 2) + 20, 200, 20), "Final Gold: " + script.gold);
        }
    }
}
