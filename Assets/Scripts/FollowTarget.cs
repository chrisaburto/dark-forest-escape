﻿using UnityEngine;
using System.Collections;

/*abstract so that we cannot create an instance of this class
 but we have to create a sub class and instantiate that*/
public abstract class FollowTarget : MonoBehaviour 
{
	/*forces Unity to serialize a private field, 
	 so that the inspector knows and saves these values 
	 even if the component is a sub class of this class*/
	[SerializeField] public Transform target;
	[SerializeField] private bool autoTargetPlayer = true;

	/*can be overrided by sub classes of this assemble*/
	virtual protected void Start () 
	{
		/*if we have the autoTargetPlayer, then call the
		 FindTargetPlayer() function*/
		if(autoTargetPlayer)
		{
			FindTargetPlayer();
		}
	}

	/*called every fixed framerate, to use when dealing
	 with rigidbody's*/
	void FixedUpdate () 
	{
		/*if we have the autoTargetPlayer, and the transform target is null, or if
		 target is not active*/
		if(autoTargetPlayer && (target == null || !target.gameObject.activeSelf))
		{
			FindTargetPlayer(); //calls function
		}
		 /*if target is null and target rigidbody is not null and is target is not kinematic*/
		if(target != null && (target.GetComponent<Rigidbody>() != null && !target.GetComponent<Rigidbody>().isKinematic))
		{
			Follow(Time.deltaTime); //calls this function 
		}
	}

	protected abstract void Follow(float deltaTime);

	public void FindTargetPlayer()
	{
		if(target = null)
		{
			GameObject targetObj = GameObject.FindGameObjectWithTag("Player");

			if(targetObj)
			{
				SetTarget(targetObj.transform);
			}
		}
	}

	public virtual void SetTarget(Transform newTransform)
	{
		target = newTransform;
	}

	public Transform Target{get {return this.target;}}
}
