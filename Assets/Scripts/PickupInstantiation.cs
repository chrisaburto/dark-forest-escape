﻿using UnityEngine;
using System.Collections;

public class PickupInstantiation : MonoBehaviour 
{
    public GameObject[] pickups;
    public bool healthInstantiated = false;
    public bool ammoInstantiated = false;
    public bool goldInstantiated = false;

    private playerActions script;
	// Use this for initialization
	void Start () 
	{
        script = GetComponent<playerActions>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	    if (transform.position.x < 0.0f)
        {
            if (!healthInstantiated)
            {
                if (script.health == 1)
                {
                    Instantiate(pickups[0], new Vector3(Random.Range(-13.0f, -2.0f), 3.0f, 55.0f), Quaternion.identity);
                    healthInstantiated = true;
                }
                if (script.health == 2)
                {
                    int f = Random.Range(0, 100);
                    int g = Random.Range(0, 25);

                    if (f == g)
                    {
                        Instantiate(pickups[0], new Vector3(Random.Range(-13.0f, -2.0f), 3.0f, 55.0f), Quaternion.identity);
                        healthInstantiated = true;
                    }
                }
            }
            if (!ammoInstantiated)
            {
                if (script.ammo <= script.originalAmmo / 2)
                {
                    Instantiate(pickups[1], new Vector3(Random.Range(-13.0f, -2.0f), 3.0f, 55.0f), Quaternion.identity);
                    ammoInstantiated = true;
                }
            }
            if (!goldInstantiated)
            {
                int f = Random.Range(0, 100);
                int g = Random.Range(0, 25);

                if (f == g)
                {
                    Instantiate(pickups[2], new Vector3(Random.Range(-13.0f, -2.0f), 3.0f, 55.0f), Quaternion.identity);
                    goldInstantiated = true;
                }
            }
        }
        if (transform.position.x > 0.0f)
        {
            if (!healthInstantiated)
            {
                if (script.health <= 1)
                {
                    Instantiate(pickups[0], new Vector3(Random.Range(3.0f, 13.0f), 3.0f, 55.0f), Quaternion.identity);
                    healthInstantiated = true;
                }
                if (script.health == 2)
                {
                    int f = Random.Range(0, 100);
                    int g = Random.Range(0, 25);

                    if (f == g)
                    {
                        Instantiate(pickups[0], new Vector3(Random.Range(3.0f, 13.0f), 3.0f, 55.0f), Quaternion.identity);
                        healthInstantiated = true;
                    }
                }
            }
            if (!ammoInstantiated)
            {
                if (script.ammo <= script.originalAmmo / 2)
                {
                    Instantiate(pickups[1], new Vector3(Random.Range(3.0f, 13.0f), 3.0f, 55.0f), Quaternion.identity);
                    ammoInstantiated = true;
                }
            }
            if (!goldInstantiated)
            {
                int f = Random.Range(0, 50);
                int g = Random.Range(0, 25);

                if (f == g)
                {
                    Instantiate(pickups[2], new Vector3(Random.Range(3.0f, 13.0f), 3.0f, 55.0f), Quaternion.identity);
                    goldInstantiated = true;
                }
            }
        }
	}
}
