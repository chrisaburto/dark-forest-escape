﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour {
    public float speed = 2.0f;

    private float velocity;
    private GameObject player;
    private PickupInstantiation script;
	
	void Start () 
	{
        velocity = speed * Time.deltaTime;
        player = GameObject.FindGameObjectWithTag("Player");
        script = player.GetComponent<PickupInstantiation>();
	}

	void Update () 
	{
        transform.position -= transform.forward * velocity;
		//transform.Rotate(new Vector3(15,30,45) * Time.deltaTime);	
        if (transform.position.z < -15)
        {
            Destroy(this.gameObject);
            script.ammoInstantiated = false;
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            script.ammoInstantiated = false;
			c.gameObject.GetComponent<playerActions>().ammo += (c.gameObject.GetComponent<playerActions>().originalAmmo / 2);
			Debug.Log ("Picked up ammo");
            Destroy(this.gameObject);
        }
    }
}
