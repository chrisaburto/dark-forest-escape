﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TerrainGenerator : MonoBehaviour {
    // Make sure to put an empty game object in from of starting section to put this script on

    //public GameObject[] section;  // Sections of terrain

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        
	}

    // To be called when ground has moved past a certain point to instantiate the next section
    void Generator ()
    {
        /*Instantiate(section[Random.Range(0, section.Length)], transform.position, transform.rotation);  // Instantiates random 
                                                                                                              // section from the array
        Debug.Log("Instantiated");*/
        GameObject obj = NewPoolerScript.current.GetPooledObject();

        if (obj == null) return;

        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        obj.SetActive(true);

        obj.transform.FindChild("Cube").gameObject.SetActive(true);
        obj.transform.FindChild("Sphere").gameObject.SetActive(true);
    }
    
    // Called current plane exits trigger this is attached to
    void OnTriggerExit (Collider c)
    {
        // Make sure each terrain is tagged as "Plane"
        if (c.tag == "Plane")
        {
                Generator();  // Calls Generator function
        }
    }
}
