﻿using UnityEngine;
using System.Collections;

public class RaycastShoot : MonoBehaviour
{
	//will use main character for the following functions, will be dragged in inspector
	public GameObject mainCam;

	//private raycast type, with Hit as identifier
	private RaycastHit Hit;

	private GameObject player;
	private playerActions script;



	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player");

		script = player.GetComponent<playerActions>();
	}

	void Update ()
	{
		if (script.ammo > 0)
		{
			Shoot();
		}
	}

	void Shoot()
	{
		//assigning a vector3 as myTransform, forward of character GameObject
		Vector3 myTransform = mainCam.transform.forward;
		Debug.Log ("shot");
		//LMB
		if(Input.GetKeyUp (KeyCode.Mouse0))
		{
			if(Physics.Raycast(transform.position, myTransform, out Hit, 25))
			{
				if(Hit.collider.gameObject.tag == "Enemy")
				{
					Debug.DrawLine(transform.position, mainCam.transform.forward, Color.yellow);
					Debug.Log ("Your Ray hit an enemy!");
					
					Hit.collider.gameObject.SetActive(false);
					Debug.Log ("You've killed an enemy!");
				}
			}
			script.ammo--;
		}
	}


}
