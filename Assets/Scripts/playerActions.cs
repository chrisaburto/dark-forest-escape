﻿using UnityEngine;
using System.Collections;

public class playerActions : MonoBehaviour 
{
    public int health = 3;
    public int ammo = 20;
	public int originalAmmo;
    public int gold = 0;
    public Vector3 moveSpeed = new Vector3(0,0,0); //allows us to to change speed via desired vector

   	public float jumpSpeed = 20.0f;  //value for players jumpSpeed in m/s

    [HideInInspector]
	public int jumpsBeforeLanding = 1; //how many times you can jump before you land

	private int jumpCount = 0;  //to count how many times you've jumped
	
	private Vector3 movement; //private movement for vector3 (x,y,z)

    [HideInInspector]
	public bool isJumpExecuted = false; //boolean to check if a jump was made

    private Rigidbody myBody; //assigning 'myBody' to a RigidBody component

	private float originalDrag; //drag of jump

	//Animator anim;//setting the animator component to identifier 'anim'
    
    void Start () 
    {
		if(!GetComponent<NetworkView>().isMine)
		{
			Component.Destroy(this);
		}

		//anim = GetComponent<Animator>();

		//giving 'myBody' a rigidbody component
        myBody = GetComponent<Rigidbody>();

		//setting out float to our rigidbody wirh drag component
        originalDrag = myBody.drag;
		originalAmmo = ammo;
	}

    void Update()
    {
		//allowing horizontal movement along the X axis
        float inputX = Input.GetAxis("Horizontal");

		//allowing movement on all 3 vectors
        Vector3 movement = new Vector3(moveSpeed.x * inputX, 0, 0);
		
        movement *= Time.deltaTime;

		//runs after above code has run, this moves the player
        transform.Translate(movement);

        //to jump every frame update
        if (Input.GetKeyDown(KeyCode.Space) && jumpCount < jumpsBeforeLanding) 
        {
            //setting bool to true (jump is enabled)
            isJumpExecuted = true; 
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
	
	void FixedUpdate () 
    {
        //if our bool is true, do the following
       if (isJumpExecuted) 
        {
            //counts our jumps
           jumpCount++;
           originalDrag = myBody.drag;

           //how fast we jump up (y-axis)
           myBody.velocity += jumpSpeed * Vector3.up;

           //setting bool to false
           isJumpExecuted = false; 
        }

        if (myBody.velocity.y < 0.0f)
        {
            myBody.drag = 0.0f;
        }
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Plane")
        {
            myBody.drag = originalDrag;

            jumpCount = 0;
        }
    }
}