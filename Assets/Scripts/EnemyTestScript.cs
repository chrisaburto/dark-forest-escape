﻿using UnityEngine;
using System.Collections;

public class EnemyTestScript : MonoBehaviour {
    public float speed = 5;

    private float velocity;
    private Rigidbody myBody;
	// Use this for initialization
	void Start () {
        velocity = speed * Time.deltaTime;
        myBody = gameObject.AddComponent<Rigidbody>();

        myBody.useGravity = false;
        myBody.freezeRotation = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.localPosition.x > 2)
        {
            myBody.AddForce(-transform.right * velocity, ForceMode.Impulse);
        }
        else if (transform.localPosition.x < -2)
        {
            myBody.AddForce(transform.right * velocity, ForceMode.Impulse);
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
        {
            c.gameObject.GetComponent<playerActions>().health--;
        }
    }
}
