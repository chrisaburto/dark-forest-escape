﻿using UnityEngine;
using System.Collections;

public class TerrainMovement : MonoBehaviour {
    public float speed = 2.0f;

    private float velocity;
    private GUIScript script;
	// Use this for initialization
	void Start () {
        velocity = speed * Time.deltaTime;
        script = GameObject.FindGameObjectWithTag("GUI").GetComponent<GUIScript>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position -= transform.forward * velocity;
	}
}
